#ifndef _OUTPUT_READER_H_
#define _OUTPUT_READER_H_
#include <iostream>
#include <string>
#include <vector>
using namespace std; 
class ScriptGlobalSymbol {
public:
	string name;
	unsigned int value;
}; 
struct SymbolTableElement {
	int _num; 
	string _name; 
	int _section; 
	bool _local; 
	int _value; 
	int _size; 
public: SymbolTableElement(int num, string name, int section,  bool local, int value, int size ) :
	_num(num), _name(name), _local(local), _section(section), _value(value), _size(size) {}; 
	SymbolTableElement() {}; 

};
struct RelocationTableElement {
	int _offset; 
	string _type; 
	int _reference;
	RelocationTableElement(int offset, string type, int reference): 
		_offset(offset), _type(type), _reference(reference) {}
};
class RelocationTable {
public:
	string sectionName; 
	vector<RelocationTableElement> _relocations; 
	int	size = 0; 
	void addRelocation(RelocationTableElement rte) {
		_relocations.push_back(rte); size++; 
	}

};
class Section {
public:
	string fileName; 
	string name; 
	int startAddr; 
	unsigned int size; 
	bool inMemory = false; 
	int num; 
	vector<char> _content;
	RelocationTable* _relocationTable;
	string type; 
	void addContent(char c) {
		_content.push_back(c); 
	}
};
class OutputScriptReader {
	
public:
	int locationCOunter; 

};
class OutputElementReader
{
	
	
public:
	string fileName;
	vector<SymbolTableElement*> _symbolTable;
	vector<Section*> _sections;
	vector<RelocationTable*> _relocations;

	OutputElementReader(string fileName) {
		this->fileName = fileName; 
	}
	vector<SymbolTableElement*> getSymbolTable() {
		return _symbolTable; 
	}

	vector<Section*> getSections() {
		return _sections; 
	}

	Section* getSection(string name); 
	SymbolTableElement* getSymbolTableElementByNum(int num); 
	SymbolTableElement* getSymbolTableElementByName(string name); 
	void addSymbolTableElement(SymbolTableElement ste); 
	Section* createNewSection(string name); 
	RelocationTable* createNewRelocationTable(string sectionName);
	unsigned int getSectionSize(string name); 
	unsigned int getSectionNum(string name); 

};

#endif // !_OUTPUT_READER_H