#pragma once
#include "OutputElementReader.h"
#include "StringParser.h"
#include <vector>
#include <string>
#include "Memory.h"

using namespace std; 
class ReaderScript
{
public:
	Memory* mem; 
	unsigned int mainAddr; 
	unsigned int locationCounter; 
	vector<Section> linkedSections;
	ReaderScript(Memory* mem);
	~ReaderScript();
	vector<ScriptGlobalSymbol> globalSymbols; 
	void combineFiles(char* scriptName, vector<OutputElementReader> objFiles);
	unsigned int executeLocationCnt(vector<string> result); 
	unsigned int executeGlobalSym(vector<string> result); 
	bool isGlobalSymbol(string symbol); 
	unsigned int findValueGlbSymbyName(string name); 
	vector<Section*> sortSectionsByName(vector<Section*> sections); 
	vector<SymbolTableElement*> linkedSymbolTable;
	bool globalDefined(string symbol); 
	unsigned int getValueGlbSymbol(string name); 
};

