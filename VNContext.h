#ifndef _VN_CONTEXT_H_
#define _VN_CONTEXT_H_
#include "Instruction.h"

#define TIMER_FLAG	0x40000000
#define Z_FLAG		0x00000001
#define O_FLAG		0x00000002
#define C_FLAG		0x00000004
#define N_FLAG		0x00000008
#define INT_FLAG	0x80000000
#define RESET_FLAGS 0xFFFFFFF0
#define CLEAR_FLAGS 0x3FFFFFFF
struct LazyFlag {
public: 
	char opCode;
	int op1, op2, result;
	bool left; 
}; 
class VNContext
{
private: 
	LazyFlag *zero, *negativ, *overflow, *carry; 
public:
	int reg[20];
	bool Z(); 
	bool N(); 
	bool O(); 
	bool C(); 
	void flags(char opCode, int op1, int op2, int result, bool left = false); 
	void setPSW(); 
	bool isEnabledTimer(); 
	bool isEnabledInt();
	void disableInterrupts(); 
	void resetFlags(); 
	VNContext();
	~VNContext();
};
#endif
