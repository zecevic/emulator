#include "Emulator.h"
#include "Errors.h"
#include <ctime>
#include <stdio.h>
#include <iostream>

using namespace std; 
void Emulator::emulate() {
//	cout<<"Ulaz 0 je "<<(unsigned)_memory->readDoubleWord(0)<<endl; 
//	cout<<"Ulaz 1 je"<<(unsigned)_memory->readDoubleWord(4)<<endl; 
//	cout<<"Ulaz 2 je"<<(unsigned)_memory->readDoubleWord(8)<<endl; 
//	cout<<"Ulaz 3 je "<<(unsigned)_memory->readDoubleWord(12)<<endl; 

	_memory->writeInIO(KB_STATUS_ADDR, 0); 
	_context->reg[PC] = _mainAddr;
	consoleOutput = false; 
	brojac = 0; 
	restartCPU(); 
	doEmulate = true;
	timerInt = false; 
	counter = 1; 
	while (doEmulate) {

		startTime = clock();

		ilegalInstruction = false; 
		unsigned int opCode = _memory->readInstruction(_context->reg[PC]);
		_context->reg[PC] += 4;
		Instruction instruction = decodeInstruction(opCode);

		executeInstruction(instruction);
		endTime	= clock();
		double eplasedTime = (endTime - startTime) / (double)(CLOCKS_PER_SEC);
		if (!timerInt)
			counter -= eplasedTime; 

		handleInterrupts(); 
	
	}
	restartCPU(); 
	cout << endl; 
//	cout<<"Vrednost registra0 je "<<_context->reg[0]<<endl; 
//	cout<<"Vrednost registra1 je: "<<_context->reg[1]<<endl; 
//	cout<<"Vrednost registra2 je: "<<_context->reg[2]<<endl; 
//	cout<<"Vrednost registra je3; "<<_context->reg[3]<<endl; 
//	cout<<"Vrednost registra 9 "<<_context->reg[9]<<endl; 
//	cout.flush(); 	
}
void Emulator::handleInterrupts(){
	if (ilegalInstruction) {
		ilegalInstruction = false; 
		_context->setPSW(); 
		push(_context->reg[PSW]); 
		_context->reg[LR] = _context->reg[PC]; 
		_context->reg[PC] = ivt->getRoutine(ILEGAL_INSTRACTION_INT); 
		if (_context->reg[PC] == 0)
			doEmulate = false; 
		cout<<"Ilegal instruction on pc: "+_context->reg[LR]<<endl; 
		return; 
	}
	if (consoleOutput) {
		consoleOutput = false; 
		int value = _memory->readFromIO(KB_OUTPUT_ADDR); 
		cout << (char)value; 
		cout.flush(); 
		return; 
	}
	if (!_context->isEnabledInt()) return; 

	if (keyboardListener->keyboardInt){
		keyboardListener->keyboardInt = 0; 
		_context->setPSW();	
		push(_context->reg[PSW]);
		push(_context->reg[LR]); 

		_context->disableInterrupts(); 
		_context->reg[LR] = _context->reg[PC];
		_context->reg[PC] = ivt->getRoutine(KB_INT);
		return; 
	}
	if (_context->isEnabledTimer() && !timerInt) {
		if (counter < 0) {
			cout << '.'; 
			timerInt = true; 
			counter += 1;
			brojac++; 
			_context->setPSW();

			push(_context->reg[PSW]);
			push(_context->reg[LR]); 

			_context->disableInterrupts(); 
			_context->reg[LR] = _context->reg[PC];
		

			_context->reg[PC] = ivt->getRoutine(TIMER_INT);
		/*	while(timerInt){
			ilegalInstruction = false; 
			unsigned int opCode = _memory->readInstruction(_context->reg[PC]);
				 
			_context->reg[PC] += 4;
			Instruction instruction = decodeInstruction(opCode);
			executeInstruction(instruction);
			}*/
		}
	}
}
int signExtend(int imm, int size) {
	
	int value; 
	int sign = 1 << (size - 1); 
	if (sign & imm) {
		int mask = 0xFFFFFFFF - ((1 << size) - 1); 
		value = imm | mask; 
	}
	else
		value = imm; 
	return value; 
}
Instruction Emulator::decodeInstruction(unsigned int ins){
	Instruction newIns;
	char cond = (ins & COND_MASK)>>29; 

	if (cond == 6) {
		ilegalInstruction = true; 
		return newIns;  
	}

	char sign = 0; 
	sign = ((ins & SIGN_MASK)>>28) & 1; 
	
	int opCode = (ins &  OPCODE_MASK) >> 24;
	if (opCode == 11){
		ilegalInstruction = true; 
		return newIns; 
	}


	newIns._con = cond; 
	newIns._sign =(int) sign; 
	newIns._opCode = opCode; 

	char src = 0;
	char dst = 0;
	char a = 0; 
	char r = 0; 
	char f = 0; 
	int imm = 0;
	bool arImm = false; // Samo za aritmeticke instrukcije
	bool load = false; //Samo za load instrukciju
	bool in = false; // samo za in/out
	bool shleft = false; //samo za mov/sh
	bool ldHigh = false; // samo za ldc 
	
	
	switch (opCode) {

	case INTR: //int 
		src = (ins & SRC_INT_MASK) >> 20;
		newIns._src = src; 
		newIns._op1 = src; 
		break;

	case ADD: case SUB: case MUL: case DIV: case CMP:

		dst = (ins & DST_ALU_MASK) >> 19;

		if (dst >= PSW){
			ilegalInstruction = true; 
			return newIns; 
		}
		if ((dst > GPR_MAX) && (opCode > SUB)) {
			ilegalInstruction = true; 
			return newIns;
		}

		newIns._dst = dst; 

		newIns._op1 = _context->reg[dst]; 
		if (ins & REG_IMM_AR_MASK) {
			//imm 
			imm = ins & IMM_AR_MASK; 
			newIns._imm = imm; 
			
			newIns._op2 = signExtend(imm, 18); 
			newIns._arImm = true; 
		}
		else {
			src = (ins & SRC_AR_MASK)>>13;

			if (src >= PSW){
				ilegalInstruction = true; 
				return newIns; 			
			}
			if ((src > GPR_MAX) && (opCode > SUB)){
				ilegalInstruction = true; 
				return newIns; 
			}

			newIns._src = src;

			newIns._op2 = _context->reg[src];
	
		}

		break; 

	case AND: case OR: case NOT: case TEST: 

		dst = (ins & DST_ALU_MASK) >> 19; 
		src = (ins & SRC_LOG_MASK) >> 14; 
		
		if ((dst > GPR_MAX && dst != SP) || (src > GPR_MAX && src != SP)){
			ilegalInstruction = true; 
			return newIns; 
		}
		
		newIns._dst = dst; 
		newIns._src = src; 

		newIns._op1 = _context->reg[dst]; 
		newIns._op2 = _context->reg[src]; 
		
		break; 

	case LDR_STR: 
		a = (ins & ADR_LDST_MASK) >> 19; 
		newIns._a = a; 
		r = (ins & REG_LDST_MASK) >> 14; 
		newIns._r = r; 
		f = (ins & F_LDST_MASK) >> 11; 
		newIns._f = f; 
		imm = ins & IMM_LDST_MASK; 
		newIns._imm = signExtend(imm,10); 

		if (a >= PSW || r > PSW){
			ilegalInstruction = true; 
			return newIns; 		
		}

		if (a == PC && f != 0){
			ilegalInstruction = true; 
			return newIns; 
		}

		if (ins & LD_ST_MASK)
			newIns._load = true;
		else
			newIns._load = false; 
		break; 


	case CALL:
		
		dst = (ins & DST_CALL_MASK) >> 19; 
		if (dst > PSW){
			ilegalInstruction = true; 
			return newIns; 
		}

		imm = ins & IMM_CALL_MASK; 
		newIns._dst = dst; 
		newIns._imm = signExtend(imm, 19); 

		break; 
	
	case IN_OUT: 
		dst = (ins & DST_IO_MASK) >> 20; 
		src = (ins & SRC_IO_MASK) >> 16; 
		
		newIns._dst = dst; 
		newIns._src = src; 
		
		if (ins & IO_MASK)
			newIns._in = true;
		else
			newIns._in = false;
		break; 
	
	case MOV_SH: 
		dst = (ins & DST_SH_MASK) >> 19;
		src = (ins & SRC_SH_MASK) >> 14;
		
		if (dst > PSW || src > PSW){
			ilegalInstruction = true; 
			return newIns;  
		}
		newIns._dst = dst; 
		newIns._src = src; 
		
		newIns._op1 = _context->reg[src];
	

		newIns._imm = (ins & IMM_SH_MASK) >> 9; 
		newIns._op2 = newIns._imm; 
		if (ins & SH_L_R_MASK)
			newIns._shleft = true; 
		break; 
	
	case LDC: 
		newIns._dst = (ins & DST_LDC_MASK) >> 20; 
	//	ldHigh = (ins & LDC_H_L_MASK ? true : false); 
		if (ins & LDC_H_L_MASK)
			newIns._ldHigh = true;
		else
			newIns._ldHigh = false; 
		newIns._imm = ins & IMM_LDC_MASK; 
		newIns._op1 = newIns._imm;
		break; 

	default: 
		ilegalInstruction = true;
	}

	return newIns; 
}
void Emulator::executeADD(Instruction ins) {
	char op = ins._opCode;
	char con = ins._con;
	int op1 = ins._op1;
	int op2 = ins._op2;
	int result; 
	bool done = false;
	if (con == AL) {
		//beuzslovno izvrsavanje
		_context->reg[ins._dst] += op2;
		done = true;
	}
	else {
		bool z,  v, n;
		switch (con) {
		case EQ:
			z = _context->Z();
			if (z) {
				result = _context->reg[ins._dst] += op2;
				done = true;
			}
			break;
		case NE:
			z = _context->Z();
			if (!z) {
				result = _context->reg[ins._dst] += op2;
				done = true;

			}
			break;
		case GT:
			z = _context->Z();
			n = _context->N();
			v = _context->O();
			if (!z && (n == v)) {
				result = _context->reg[ins._dst] += op2;
				done = true;
			}
			break;
		case GE:
			n = _context->N();
			v = _context->O(); 
			if (n == v){
				result = _context->reg[ins._dst] +=op2; 
				done = true; 
			} 
			break; 
		case LT:
			n = _context->N();
			v = _context->O();
			if (n != v) {
				result = _context->reg[ins._dst] += op2;
				done = true;
			}
			break; 
		case LE: 
			z = _context->Z();
			n = _context->N();
			v = _context->O();
			if (z || (n != v)) {
				result = _context->reg[ins._dst] += op2;
				done = true;
			}
			break;
		}
	}
	if (ins._sign && done) {
		_context->flags(ins._opCode, op1, op2, result); 
	}
}
void Emulator::executeSUB(Instruction ins) {
	char op = ins._opCode;
	char con = ins._con;
	int op1 = ins._op1;
	int op2 = ins._op2;
	int result;
	bool done = false;
	if (con == AL) {
		//beuzslovno izvrsavanje
		_context->reg[ins._dst] -= op2;
		done = true;
	}
	else {
		bool z, v, n;
		switch (con) {
		case EQ:
			z = _context->Z();
			if (z) {
				result = _context->reg[ins._dst] -= op2;
				done = true;
			}
			break;
		case NE:
			z = _context->Z();
			if (!z) {
				result = _context->reg[ins._dst] -= op2;
				done = true;

			}
			break;
		case GT:
			z = _context->Z();
			n = _context->N();
			v = _context->O();
			if (!z && (n == v)) {
				result = _context->reg[ins._dst] -= op2;
				done = true;
			}
			break;
		case GE:
			n = _context->N();
			v = _context->O(); 
			if (n == v){
				result = _context->reg[ins._dst] -=op2; 
				done = true; 
			} 
			break; 
		case LT:
			n = _context->N();
			v = _context->O();
			if (n != v) {
				result = _context->reg[ins._dst] -= op2;
				done = true;
			}
			break;
		case LE:
			z = _context->Z();
			n = _context->N();
			v = _context->O();
			if (z || (n != v)) {
				result = _context->reg[ins._dst] -= op2;
				done = true;
			}
			break;
		}
	}
	if (ins._sign && done) {
		_context->flags(ins._opCode, op1, op2, result);
	}
}
void Emulator::executeMUL(Instruction ins) {
	char op = ins._opCode;
	char con = ins._con;
	int op1 = ins._op1;
	int op2 = ins._op2;
	int result;
	bool done = false;
	if (con == AL) {
		//beuzslovno izvrsavanje
		_context->reg[ins._dst] *= op2;
		done = true;
	}
	else {
		bool z, v,  n;
		switch (con) {
		case EQ:
			z = _context->Z();
			if (z) {
				result = _context->reg[ins._dst] *= op2;
				done = true;
			}
			break;
		case NE:
			z = _context->Z();
			if (!z) {
				result = _context->reg[ins._dst] *= op2;
				done = true;

			}
			break;
		case GT:
			z = _context->Z();
			n = _context->N();
			v = _context->O();
			if (!z && (n == v)) {
				result = _context->reg[ins._dst] *= op2;
				done = true;
			}
			break;
		case GE:
			n = _context->N();
			v = _context->O(); 
			if (n == v){
				result = _context->reg[ins._dst] *=op2; 
				done = true; 
			} 
			break; 
		case LT:
			n = _context->N();
			v = _context->O();
			if (n != v) {
				result = _context->reg[ins._dst] *= op2;
				done = true;
			}
			break;
		case LE:
			z = _context->Z();
			n = _context->N();
			v = _context->O();
			if (z || (n != v)) {
				result = _context->reg[ins._dst] *= op2;
				done = true;
			}
			break;
		}
	}
	if (ins._sign && done) {
		_context->flags(ins._opCode, op1, op2, result);
	}
}
void Emulator::executeDIV(Instruction ins) {
	char op = ins._opCode;
	char con = ins._con;
	int op1 = ins._op1;
	int op2 = ins._op2;
	int result;
	bool done = false;
	if (op2 == 0) {
		ilegalInstruction = true; 
		return; 
	}
	if (con == AL) {
		//beuzslovno izvrsavanje
		_context->reg[ins._dst] /= op2;
		done = true;
	}
	else {
		bool z, c, v, o, n;
		switch (con) {
		case EQ:
			z = _context->Z();
			if (z) {
				result = _context->reg[ins._dst] /= op2;
				done = true;
			}
			break;
		case NE:
			z = _context->Z();
			if (!z) {
				result = _context->reg[ins._dst] /= op2;
				done = true;

			}
			break;
		case GT:
			z = _context->Z();
			n = _context->N();
			v = _context->O();
			if (!z && (n == v)) {
				result = _context->reg[ins._dst] /= op2;
				done = true;
			}
			break;
		case GE:
			n = _context->N();
			v = _context->O(); 
			if (n == v){
				result = _context->reg[ins._dst] /=op2; 
				done = true; 
			} 
			break; 
		case LT:
			n = _context->N();
			v = _context->O();
			if (n != v) {
				result = _context->reg[ins._dst] /= op2;
				done = true;
			}
			break;
		case LE:
			z = _context->Z();
			n = _context->N();
			v = _context->O();
			if (z || (n != v)) {
				result = _context->reg[ins._dst] /= op2;
				done = true;
			}
			break;
		}
	}
	if (ins._sign && done) {
		_context->flags(ins._opCode, op1, op2, result);
	}
}
void Emulator::executeCMP(Instruction ins) {
	char op = ins._opCode;
	char con = ins._con;
	int op1 = ins._op1;
	int op2 = ins._op2;
	int result;
	bool done = false;
	if (con == AL) {
		//beuzslovno izvrsavanje
		result = op1 - op2;
		done = true;
	}
	else {
		bool z,  v, n;
		switch (con) {
		case EQ:
			z = _context->Z();
			if (z) {
				result = op1 - op2;
				done = true;
			}
			break;
		case NE:
			z = _context->Z();
			if (!z) {
				result = op1 - op2;
				done = true;

			}
			break;
		case GT:
			z = _context->Z();
			n = _context->N();
			v = _context->O();
			if (!z && (n == v)) {
				result = op1 - op2;
				done = true;
			}
			break;
		case GE:
			n = _context->N();
			v = _context->O(); 
			if (n == v){
				result = op1 - op2; 
				done = true; 
			} 
			break; 
		case LT:
			n = _context->N();
			v = _context->O();
			if (n != v) {
				result = op1 - op2;
				done = true;
			}
			break;
		case LE:
			z = _context->Z();
			n = _context->N();
			v = _context->O();
			if (z || (n != v)) {
				result = op1 - op2;
				done = true;
			}
			break;
		}
	}
	if (ins._sign && done) {
		_context->flags(ins._opCode, op1, op2, result);
	}
}
void Emulator::executeAND(Instruction ins) {
	char op = ins._opCode;
	char con = ins._con;
	int op1 = ins._op1;
	int op2 = ins._op2;
	int result;
	bool done = false;
	if (con == AL) {
		//beuzslovno izvrsavanje
		_context->reg[ins._dst] &= op2;
		done = true;
	}
	else {
		bool z, v, n;
		switch (con) {
		case EQ:
			z = _context->Z();
			if (z) {
				result = _context->reg[ins._dst] &= op2;
				done = true;
			}
			break;
		case NE:
			z = _context->Z();
			if (!z) {
				result = _context->reg[ins._dst] &= op2;
				done = true;

			}
			break;
		case GT:
			z = _context->Z();
			n = _context->N();
			v = _context->O();
			if (!z && (n == v)) {
				result = _context->reg[ins._dst] &= op2;
				done = true;
			}
			break;
		case GE:
			n = _context->N();
			v = _context->O(); 
			if (n == v){
				result = _context->reg[ins._dst] &=op2; 
				done = true; 
			} 
			break; 
		case LT:
			n = _context->N();
			v = _context->O();
			if (n != v) {
				result = _context->reg[ins._dst] &= op2;
				done = true;
			}
			break;
		case LE:
			z = _context->Z();
			n = _context->N();
			v = _context->O();
			if (z || (n != v)) {
				result = _context->reg[ins._dst] &= op2;
				done = true;
			}
			break;
		}
	}
	if (ins._sign && done) {
		_context->flags(ins._opCode, op1, op2, result);
	}
}
void Emulator::executeOR(Instruction ins) {
	char op = ins._opCode;
	char con = ins._con;
	int op1 = ins._op1;
	int op2 = ins._op2;
	int result;
	bool done = false;
	if (con == AL) {
		//beuzslovno izvrsavanje
		_context->reg[ins._dst] |= op2;
		done = true;
	}
	else {
		bool z, v, n;
		switch (con) {
		case EQ:
			z = _context->Z();
			if (z) {
				result = _context->reg[ins._dst] |= op2;
				done = true;
			}
			break;
		case NE:
			z = _context->Z();
			if (!z) {
				result = _context->reg[ins._dst] |= op2;
				done = true;

			}
			break;
		case GT:
			z = _context->Z();
			n = _context->N();
			v = _context->O();
			if (!z && (n == v)) {
				result = _context->reg[ins._dst] |= op2;
				done = true;
			}
			break;
		case GE:
			n = _context->N();
			v = _context->O(); 
			if (n == v){
				result = _context->reg[ins._dst] |=op2; 
				done = true; 
			} 
			break; 
		case LT:
			n = _context->N();
			v = _context->O();
			if (n != v) {
				result = _context->reg[ins._dst] += op2;
				done = true;
			}
			break;
		case LE:
			z = _context->Z();
			n = _context->N();
			v = _context->O();
			if (z || (n != v)) {
				result = _context->reg[ins._dst] |= op2;
				done = true;
			}
			break;
		}
	}
	if (ins._sign && done) {
		_context->flags(ins._opCode, op1, op2, result);
	}
}
void Emulator::executeNOT(Instruction ins) {
	char op = ins._opCode;
	char con = ins._con;
	int op1 = ins._op1;
	int op2 = ins._op2;
	int result;
	bool done = false;
	if (con == AL) {
		//beuzslovno izvrsavanje
		_context->reg[ins._dst] =~op2;
		done = true;
	}
	else {
		bool z, v,  n;
		switch (con) {
		case EQ:
			z = _context->Z();
			if (z) {
				result = _context->reg[ins._dst] = ~op2;
				done = true;
			}
			break;
		case NE:
			z = _context->Z();
			if (!z) {
				result = _context->reg[ins._dst] = ~op2;
				done = true;

			}
			break;
		case GT:
			z = _context->Z();
			n = _context->N();
			v = _context->O();
			if (!z && (n == v)) {
				result = _context->reg[ins._dst] = ~op2;
				done = true;
			}
			break;
		case GE:
			n = _context->N();
			v = _context->O(); 
			if (n == v){
				result = _context->reg[ins._dst] = ~op2; 
				done = true; 
			} 
			break; 
		case LT:
			n = _context->N();
			v = _context->O();
			if (n != v) {
				result = _context->reg[ins._dst] = ~op2;
				done = true;
			}
			break;
		case LE:
			z = _context->Z();
			n = _context->N();
			v = _context->O();
			if (z || (n != v)) {
				result = _context->reg[ins._dst] = ~op2;
				done = true;
			}
			break;
		}
	}
	if (ins._sign && done) {
		_context->flags(ins._opCode, op1, op2, result);
	}
}
void Emulator::executeTEST(Instruction ins) {
	char op = ins._opCode;
	char con = ins._con;
	int op1 = ins._op1;
	int op2 = ins._op2;
	int result;
	bool done = false;
	if (con == AL) {
		//beuzslovno izvrsavanje
		result = op1 & op2;
		done = true;
	}
	else {
		bool z,  v,  n;
		switch (con) {
		case EQ:
			z = _context->Z();
			if (z) {
				result = op1 & op2;
				done = true;
			}
			break;
		case NE:
			z = _context->Z();
			if (!z) {
				result = op1 & op2;
				done = true;

			}
			break;
		case GT:
			z = _context->Z();
			n = _context->N();
			v = _context->O();
			if (!z && (n == v)) {
				result = op1 & op2;
				done = true;
			}
			break;
		case GE:
			n = _context->N();
			v = _context->O(); 
			if (n == v){
				result = op1 & op2; 
				done = true; 
			} 
			break; 
		case LT:
			n = _context->N();
			v = _context->O();
			if (n != v) {
				result = op1 & op2;
				done = true;
			}
			break;
		case LE:
			z = _context->Z();
			n = _context->N();
			v = _context->O();
			if (z || (n != v)) {
				result = op1 & op2;
				done = true;
			}
			break;
		}
	}
	if (ins._sign && done) {
		_context->flags(ins._opCode, op1, op2, result);
	}
}
void Emulator::executeLDR_STR(Instruction ins) {
	char op = ins._opCode;
	char con = ins._con;
	int r = ins._r; 
	int a = ins._a; 
	unsigned address; 
	bool done = false;

	

	if (con == AL) {

		if (ins._f == PRE_INC_MEM)
			_context->reg[a] += 4;
		else if (ins._f == PRE_DEC_MEM)
			_context->reg[a] -= 4;

		address = _context->reg[a] + ins._imm; 
		if (a == PC)
			address += 4; 
		if (ins._load)
			_context->reg[r] = _memory->readDoubleWord(address);
		else
			_memory->writeDoubleWord(address, _context->reg[r]); 

		done = true;
		if (ins._f == POST_INC_MEM)
			_context->reg[a] += 4;
		else if (ins._f == POST_DEC_MEM)
			_context->reg[a] -= 4; 
	}
	else {
		bool z, v, n;
		switch (con) {
		case EQ:
			z = _context->Z();
			if (z) {
				if (ins._f == PRE_INC_MEM)
					_context->reg[a] += 4;
				else if (ins._f == PRE_DEC_MEM)
					_context->reg[a] += 4;
				address = _context->reg[a] + ins._imm;
				if (a == PC)
					address += 4;

				if (ins._load)
					_context->reg[r] = _memory->readDoubleWord(address);
				else
					_memory->writeDoubleWord(address, _context->reg[r]);

				done = true;
				if (ins._f == POST_INC_MEM)
					_context->reg[a] += 4;
				else if (ins._f == POST_DEC_MEM)
					_context->reg[a] -= 4;
			}
			break;
		case NE:
			z = _context->Z();
			if (!z) {
				if (ins._f == PRE_INC_MEM)
					_context->reg[a] += 4;
				else if (ins._f == PRE_DEC_MEM)
					_context->reg[a] += 4;
				address = _context->reg[a] + ins._imm;
				if (a == PC)
					address += 4;

				if (ins._load)
					_context->reg[r] = _memory->readDoubleWord(address);
				else
					_memory->writeDoubleWord(address, _context->reg[r]);

				done = true;
				if (ins._f == POST_INC_MEM)
					_context->reg[a] += 4;
				else if (ins._f == POST_DEC_MEM)
					_context->reg[a] -= 4;

			}
			break;
		case GT:
			z = _context->Z();
			n = _context->N();
			v = _context->O();
			if (!z && (n == v)) {
				if (ins._f == PRE_INC_MEM)
					_context->reg[a] += 4;
				else if (ins._f == PRE_DEC_MEM)
					_context->reg[a] += 4;
				address = _context->reg[a] + ins._imm;
				if (a == PC)
					address += 4;

				if (ins._load)
					_context->reg[r] = _memory->readDoubleWord(address);
				else
					_memory->writeDoubleWord(address, _context->reg[r]);

				done = true;
				if (ins._f == POST_INC_MEM)
					_context->reg[a] += 4;
				else if (ins._f == POST_DEC_MEM)
					_context->reg[a] -= 4;
			}
			break;
		case GE:
			n = _context->N();
			v = _context->O(); 
			if (n == v){
				if (ins._f == PRE_INC_MEM)
					_context->reg[a] += 4;
				else if (ins._f == PRE_DEC_MEM)
					_context->reg[a] += 4;
				address = _context->reg[a] + ins._imm;
				if (a == PC)
					address += 4;

				if (ins._load)
					_context->reg[r] = _memory->readDoubleWord(address);
				else
					_memory->writeDoubleWord(address, _context->reg[r]);

				done = true;
				if (ins._f == POST_INC_MEM)
					_context->reg[a] += 4;
				else if (ins._f == POST_DEC_MEM)
					_context->reg[a] -= 4; 
			} 
			break; 
		case LT:
			n = _context->N();
			v = _context->O();
			if (n != v) {
				if (ins._f == PRE_INC_MEM)
					_context->reg[a] += 4;
				else if (ins._f == PRE_DEC_MEM)
					_context->reg[a] += 4;
				address = _context->reg[a] + ins._imm;
				if (a == PC)
					address += 4;

				if (ins._load)
					_context->reg[r] = _memory->readDoubleWord(address);
				else
					_memory->writeDoubleWord(address, _context->reg[r]);

				done = true;
				if (ins._f == POST_INC_MEM)
					_context->reg[a] += 4;
				else if (ins._f == POST_DEC_MEM)
					_context->reg[a] -= 4;
			}
			break;
		case LE:
			z = _context->Z();
			n = _context->N();
			v = _context->O();
			if (z || (n != v)) {
				if (ins._f == PRE_INC_MEM)
					_context->reg[a] += 4;
				else if (ins._f == PRE_DEC_MEM)
					_context->reg[a] += 4;
				address = _context->reg[a] + ins._imm;
				if (a == PC)
					address += 4;

				if (ins._load)
					_context->reg[r] = _memory->readDoubleWord(address);
				else
					_memory->writeDoubleWord(address, _context->reg[r]);

				done = true;
				if (ins._f == POST_INC_MEM)
					_context->reg[a] += 4;
				else if (ins._f == POST_DEC_MEM)
					_context->reg[a] -= 4;
			}
			break;
		}
	}
}
void Emulator::executeCALL(Instruction ins){

	char con = ins._con;

	bool done = false;
	if (con == AL) {
		//beuzslovno izvrsavanje
		_context->reg[LR] = _context->reg[PC]; 
		
		if (ins._dst == PC)
			_context->reg[PC] = _context->reg[ins._dst] + ins._imm+4;
		else
			_context->reg[PC] = _context->reg[ins._dst] + ins._imm;
		done = true;
	}
	else {
		bool z,  v,  n;
		switch (con) {
		case EQ:
			z = _context->Z();
			if (z) {
				_context->reg[LR] = _context->reg[PC];
				if (ins._dst == PC)
					_context->reg[PC] = _context->reg[ins._dst] + ins._imm + 4;
				else
					_context->reg[PC] = _context->reg[ins._dst] + ins._imm;
				done = true;
			}
			break;
		case NE:
			z = _context->Z();
			if (!z) {
				_context->reg[LR] = _context->reg[PC];
				if (ins._dst == PC)
					_context->reg[PC] = _context->reg[ins._dst] + ins._imm + 4;
				else
					_context->reg[PC] = _context->reg[ins._dst] + ins._imm;
				done = true;

			}
			break;
		case GT:
			z = _context->Z();
			n = _context->N();
			v = _context->O();
			if (!z && (n == v)) {
				_context->reg[LR] = _context->reg[PC];
				if (ins._dst == PC)
					_context->reg[PC] = _context->reg[ins._dst] + ins._imm + 4;
				else
					_context->reg[PC] = _context->reg[ins._dst] + ins._imm;
				done = true;
			}
			break;
		case GE:
			n = _context->N();
			v = _context->O(); 
			if (n == v){
				_context->reg[LR] = _context->reg[PC];
				if (ins._dst == PC)
					_context->reg[PC] = _context->reg[ins._dst] + ins._imm + 4;
				else
					_context->reg[PC] = _context->reg[ins._dst] + ins._imm;
				done = true;
			}
			break; 
		case LT:
			n = _context->N();
			v = _context->O();
			if (n != v) {
				_context->reg[LR] = _context->reg[PC];
				if (ins._dst == PC)
					_context->reg[PC] = _context->reg[ins._dst] + ins._imm + 4;
				else
					_context->reg[PC] = _context->reg[ins._dst] + ins._imm;
				done = true;
			}
			break;
		case LE:
			z = _context->Z();
			n = _context->N();
			v = _context->O();
			if (z || (n != v)) {
				_context->reg[LR] = _context->reg[PC];
				if (ins._dst == PC)
					_context->reg[PC] = _context->reg[ins._dst] + ins._imm + 4; //Jer je u asm posmatrano kao da je PC za 4 veci
				else
					_context->reg[PC] = _context->reg[ins._dst] + ins._imm;
				done = true;
			}
			break;
		}
	}
	 	
}
void Emulator::executeIN_OUT(Instruction ins){
	char op = ins._opCode; 
	char con = ins._con; 
	int op1 = ins._op1; 
	int op2 = ins._op2; 
	bool done = false;
	unsigned int address = _context->reg[ins._src]; 
	if (con == AL) {
		//beuzslovno izvrsavanje
		if (ins._in) {			
		//	if (address == IO_IN_ADDR){
		//		while (!_memory->getKBStatus()); 					
		//	}
			_context->reg[ins._dst] = _memory->readFromIO(address);
			if (address == KB_INPUT_ADDR){
				
				_memory->writeInIO(KB_STATUS_ADDR, 0); 
			}
		}
		else {
			
			_memory->writeInIO(address, _context->reg[ins._dst]);
			if (address == IO_OUT_ADDR)
				consoleOutput = true; 
		}
			done = true;
	}
	else {
		bool z,  v,  n;
		switch (con) {
		case EQ:
			z = _context->Z();
			if (z) {
				if (ins._in){
					
					_context->reg[ins._dst] = _memory->readFromIO(address);
					if (address == KB_INPUT_ADDR){
				
						_memory->writeInIO(KB_STATUS_ADDR, 0); 
					}
				//	_memory->writeInIO(KB_STATUS_ADDR, 0);
				}
				else
				{
					_memory->writeInIO(address, _context->reg[ins._dst]);
					if (address == IO_OUT_ADDR)
						consoleOutput = true;
				}
				done = true;
			}
			break;
		case NE:
			z = _context->Z();
			if (!z) {
				if (ins._in) {
										
					_context->reg[ins._dst] = _memory->readFromIO(address);
					if (address == KB_INPUT_ADDR){
				
						_memory->writeInIO(KB_STATUS_ADDR, 0); 
					}
				}
				else {

					_memory->writeInIO(address, _context->reg[ins._dst]);
					if (address == IO_OUT_ADDR)
						consoleOutput = true;
				}
				done = true;

			}
			break;
		case GT:
			z = _context->Z();
			n = _context->N();
			v = _context->O();
			if (!z && (n == v)) {
				if (ins._in) {										
					_context->reg[ins._dst] = _memory->readFromIO(address);
					if (address == KB_INPUT_ADDR){
				
						_memory->writeInIO(KB_STATUS_ADDR, 0); 
					}
				}
				else {

					_memory->writeInIO(address, _context->reg[ins._dst]);
					if (address == IO_OUT_ADDR)
						consoleOutput = true;
				}
				done = true;
			}
			break;
		case GE:
			n = _context->N();
			v = _context->O(); 
			if (n == v){
				if (ins._in) {
				//	if (address == IO_IN_ADDR)
				//		while (!_memory->getKBStatus());
									
					_context->reg[ins._dst] = _memory->readFromIO(address);
					if (address == KB_INPUT_ADDR){
				
						_memory->writeInIO(KB_STATUS_ADDR, 0); 
					}
				}
				else {

					_memory->writeInIO(address, _context->reg[ins._dst]);
					if (address == IO_OUT_ADDR)
						consoleOutput = true;
				}
				done = true;
			}
			break; 
		case LT:
			n = _context->N();
			v = _context->O();
			if (n != v) {
				if (ins._in) {
					
					_context->reg[ins._dst] = _memory->readFromIO(address);
					if (address == KB_INPUT_ADDR){
				
						_memory->writeInIO(KB_STATUS_ADDR, 0); 
					}
				}
				else {

					_memory->writeInIO(address, _context->reg[ins._dst]);
					if (address == IO_OUT_ADDR)
						consoleOutput = true;
				}
				done = true;
			}
			break;
		case LE:
			z = _context->Z();
			n = _context->N();
			v = _context->O();
			if (z || (n != v)) {
				if (ins._in) {
					
					_context->reg[ins._dst] = _memory->readFromIO(address);
					if (address == KB_INPUT_ADDR){
				
						_memory->writeInIO(KB_STATUS_ADDR, 0); 
					}
				}
				else
				{

					_memory->writeInIO(address, _context->reg[ins._dst]);
					if (address == IO_OUT_ADDR)
						consoleOutput = true;
				}
				done = true;
			}
			break;
		}
	}	
}
void Emulator::executeMOV_SH(Instruction ins){
	char op = ins._opCode;
	char con = ins._con;
	int op1 = ins._op1;
	int op2 = ins._op2;
	int result;
	bool done = false;
	if (con == AL) {
		//beuzslovno izvrsavanje
		if (ins._shleft)
			result = _context->reg[ins._dst] = op1 << op2;
		else
			result = _context->reg[ins._dst] = op1 >> op2; 
		done = true;
	}
	else {
		bool z, c, v, o, n;
		switch (con) {
		case EQ:
			z = _context->Z();
			if (z) {
				if (ins._shleft)
					result = _context->reg[ins._dst] = op1 << op2;
				else
					result = _context->reg[ins._dst] = op1 >> op2;
				done = true; 
			}
			break;
		case NE:
			z = _context->Z();
			if (!z) {
				if (ins._shleft)
					result = _context->reg[ins._dst] = op1 << op2;
				else
					result = _context->reg[ins._dst] = op1 >> op2;
				done = true; 

			}
			break;
		case GT:
			z = _context->Z();
			n = _context->N();
			v = _context->O();
			if (!z && (n == v)) {
				if (ins._shleft)
					result = _context->reg[ins._dst] = op1 << op2;
				else
					result = _context->reg[ins._dst] = op1 >> op2;
				done = true;
			}
			break;
		case GE:
			n = _context->N(); 
			v = _context->O(); 
			if (n == v){
				if (ins._shleft)
					result = _context->reg[ins._dst] = op1 << op2;
				else
					result = _context->reg[ins._dst] = op1 >> op2;
				done = true;
			}
			break; 
		case LT:
			n = _context->N();
			v = _context->O();
			if (n != v) {
				if (ins._shleft)
					result = _context->reg[ins._dst] = op1 << op2;
				else
					result = _context->reg[ins._dst] = op1 >> op2;
				done = true;
			}
			break;
		case LE:
			z = _context->Z();
			n = _context->N();
			v = _context->O();
			if (z || (n != v)) {
				if (ins._shleft)
					result = _context->reg[ins._dst] = op1 << op2;
				else
					result = _context->reg[ins._dst] = op1 >> op2;
				done = true;
			}
			break;
		}
	}
	if (ins._sign && done && ins._dst!=PC) {
		_context->flags(ins._opCode, op1, op2, result, ins._shleft);
	}
	if (ins._sign==1 && ins._dst == PC) {
		if (!initializing) {
			_context->reg[LR] = pop();
			_context->reg[PSW] = pop();
		}

		if (initializing) {
			initializing = false; 
		}
		if (timerInt)
			timerInt = false; 
	}
}
void Emulator::executeLDC(Instruction ins){
	char op = ins._opCode;
	char con = ins._con;
	int op1 = ins._op1;
	bool high = ins._ldHigh; 
	bool done = false;
	if (con == AL) {
		//beuzslovno izvrsavanje
		if (high) {
			unsigned int val = op1 << 16; 
			_context->reg[ins._dst] |= op1 << 16;
		}
		else
			_context->reg[ins._dst] = op1; 
		
		done = true;
	}
	else {
		bool z, c, v, n;
		switch (con) {
		case EQ:
			z = _context->Z();
			if (z) {
				if (high)
					_context->reg[ins._dst] |= op1 << 16;
				else
					_context->reg[ins._dst] = op1;
				done = true;
			}
			break;
		case NE:
			z = _context->Z();
			if (!z) {
				if (high)
					_context->reg[ins._dst] |= op1 << 16;
				else
					_context->reg[ins._dst] = op1;
				done = true;

			}
			break;
		case GT:
			z = _context->Z();
			n = _context->N();
			v = _context->O();
			if (!z && (n == v)) {
				if (high)
					_context->reg[ins._dst] |= op1 << 16;
				else
					_context->reg[ins._dst] = op1;
				done = true;
			}
			break;
		case GE:
			n = _context->N(); 
			v = _context->O(); 
			if (n == v){
					if (high)
					_context->reg[ins._dst] |= op1 << 16;
				else
					_context->reg[ins._dst] = op1;
				done = true;
			}
		break; 
		case LT:
			n = _context->N();
			v = _context->O();
			if (n != v) {
				if (high)
					_context->reg[ins._dst] |= op1 << 16;
				else
					_context->reg[ins._dst] = op1;
				done = true;
			}
			break;
		case LE:
			z = _context->Z();
			n = _context->N();
			v = _context->O();
			if (z || (n != v)) {
				if (high)
					_context->reg[ins._dst] |= op1 << 16;
				else
					_context->reg[ins._dst] = op1;
				done = true;
			}
			break;
		}
	}
}
int  Emulator::pop(){
	unsigned int address = _context->reg[SP]; 
	unsigned int doubleWord = _memory->popDoubleWord(address); 
	_context->reg[SP] += 4; 
	return doubleWord; 

}
void Emulator::push(int doubleWord){

	unsigned int address = _context->reg[SP]; 
	_memory->pushDoubleWord(doubleWord, address); 
	_context->reg[SP] -= 4; 
}
void Emulator::restartCPU(){
	int i =0; 

//	_context->setPSW();

	_memory->writeInIO(KB_STATUS_ADDR, 0); 
	
//	push(_context->reg[PSW]);
//	push(_context->reg[LR]); 


	_context->reg[LR] = _context->reg[PC];

	_context->reg[PC] = ivt->getRoutine(RESET_INT); 

	initializing = true; 
	while (initializing) {
		ilegalInstruction = false; 
		unsigned int opCode = _memory->readInstruction(_context->reg[PC]);
		 
		_context->reg[PC] += 4;
		Instruction instruction = decodeInstruction(opCode);
		executeInstruction(instruction);
	}
}

void Emulator::executeInstruction(Instruction ins){ 
	if (ilegalInstruction) return; 
	int op = ins._opCode; 
	switch (op)
	{
	case INTR:		executeINT(ins);		break;
	case ADD:		executeADD(ins);		break;
	case SUB:		executeSUB(ins);		break; 
	case MUL:		executeMUL(ins);		break; 
	case DIV:		executeDIV(ins);		break; 
	case CMP:		executeCMP(ins);		break;
	case AND:		executeAND(ins);		break; 
	case OR:		executeOR(ins);			break; 
	case NOT:		executeNOT(ins);		break; 
	case TEST: 		executeTEST(ins);		break; 
	case LDR_STR:	executeLDR_STR(ins);	break; 
	case CALL:		executeCALL(ins);		break; 
	case IN_OUT:	executeIN_OUT(ins);		break; 
	case MOV_SH:	executeMOV_SH(ins);		break; 
	case LDC:		executeLDC(ins);		break; 
	default: 
		break;
	}
	

}
void Emulator::executeINT(Instruction ins){

	unsigned int num = ins._src; 
	if (num >= 16)
		ilegalInstruction = true; 

	if (num == RESET_INT) {
		doEmulate = false;
	}
	else {
		_context->setPSW();
		push(_context->reg[PSW]);
		push(_context->reg[LR]);
		_context->disableInterrupts();
		_context->reg[LR] = _context->reg[PC];
		_context->reg[PC] = ivt->getRoutine(num);
	}

}
