#ifndef  _STRING_PARSER_H_
#define	 _STRING_PARSER_H_
#include <iostream>
#include <string>
#include <list>
#include <vector>
#include "OutputElementReader.h"
using namespace std;
enum Code { SymbolTableId, RelocationTableId, SectionId, WrongId};
enum CodeScript { GlobalSymbol, LocationCounter, AlignDirective, SectionOrder};
class StringParser
{
public:
	StringParser();
	~StringParser();

	bool isSeparator(char c); 
	string getWord(string& line); 
	vector<string> parseLine(string line); 
	Code decode(string word); 
	SymbolTableElement createSymbolTableElement(vector<string> params); 
	RelocationTableElement createRelocationTableElement(vector<string> params); 
	string getSectionNameFromRel(vector<string> params); 
	string getSectionNameFromSec(vector<string> params);
	CodeScript decodeScript(string word); 
	unsigned int parseToInt(string num); 
	bool isAlignOp(vector<string> params); 
	unsigned int from10(string num); 
	unsigned int from8(string num); 
	unsigned int from16(string num); 
	unsigned int from2(string num); 
	unsigned int getSectionSize(vector<string> params); 
	bool allNum(string num);


};

#endif // ! _STRING_PARSER_H_
