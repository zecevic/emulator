#include "KeyboardListener.h"
#include <iostream>
using namespace std; 



KeyboardListener::KeyboardListener(Memory* memory)
{
	this->memory = memory; 
	keyboardInt = false; 

}


KeyboardListener::~KeyboardListener()
{
}

void* keyboardListenerFunction(void* keyboardListenerV) {

	cout.flush(); 
	KeyboardListener* keyboardListener = (KeyboardListener*)keyboardListenerV; 
	char c; 
	while (1) {
		cin >> c; 
		keyboardListener->memory->writeInIO(KB_INPUT_ADDR, c); 
		keyboardListener->memory->writeInIO(KB_STATUS_ADDR, KB_CHAR_READY); 
		keyboardListener->keyboardInt = true; 
		while (keyboardListener->keyboardInt); 
	//	keyboardListener->memory->writeInIO(KB_STATUS_ADDR, 0);
	}
}