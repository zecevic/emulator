#ifndef _READER_OBJ_H_
#define _READER_OBJ_H_

#include "OutputElementReader.h"
#include <string>
using namespace std; 
class ReaderObj
{

public:
	ReaderObj();
	~ReaderObj();

	OutputElementReader readFile(char* fileName); 
};

#endif // !_READER_OBJ_H_