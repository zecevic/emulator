#include "OutputElementReader.h"
#include "Errors.h"
Section* OutputElementReader::getSection(string name)
{
	for (int i = 0; i < _sections.size(); i++)
		if (_sections[i]->name == name) return _sections[i]; 
	return nullptr; 
}

SymbolTableElement* OutputElementReader::getSymbolTableElementByNum(int num)
{
	for (int i = 0; i < _symbolTable.size(); i++)
		if (_symbolTable[i]->_num == num) return _symbolTable[i]; 
	return nullptr; 
}

SymbolTableElement * OutputElementReader::getSymbolTableElementByName(string name)
{
	for (int i = 0; i < _symbolTable.size(); i++)
		if (_symbolTable[i]->_name == name) return _symbolTable[i]; 
	return nullptr;
}

void OutputElementReader::addSymbolTableElement(SymbolTableElement ste)
{
	SymbolTableElement* s = new SymbolTableElement(); 
	s->_section = ste._section; 
	s->_local = ste._local; 
	s->_name = ste._name; 
	s->_num = ste._num; 
	s->_value = ste._value; 
	s->_size = ste._size; 
	_symbolTable.push_back(s); 
}

Section* OutputElementReader::createNewSection(string name)
{
	Section* s = new Section(); 
	s->name = name;
	unsigned int size = getSectionSize(name); 
	s->size = size; 
	_sections.push_back(s); 
	return s; 
}

RelocationTable* OutputElementReader::createNewRelocationTable(string sectionName)
{
	RelocationTable* r = new RelocationTable(); 
	r->sectionName = sectionName; 
	_relocations.push_back(r); 
	Section* s = getSection(sectionName); 
	s->_relocationTable = r; 
	return _relocations[_relocations.size() - 1];
}
unsigned int OutputElementReader::getSectionSize(string name){
		for (int i = 0; i < _symbolTable.size(); i++)
		if (_symbolTable[i]->_name == name) return _symbolTable[i]->_size; 
		throw new Error("Nema sekcije sa imenom: "+name); 
}

unsigned int OutputElementReader::getSectionNum(string name)
{
	for (int i = 0; i < _symbolTable.size(); i++)
		if (_symbolTable[i]->_name == name) return _symbolTable[i]->_num;
	throw new Error("Nema sekcije sa imenom: " + name);
}
