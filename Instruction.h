#pragma once


#define PRE_INC_MEM		0x00000004
#define PRE_DEC_MEM		0x00000005
#define POST_INC_MEM	0x00000002
#define POST_DEC_MEM	0x00000003
#define COND_MASK		0xE0000000
#define SIGN_MASK		0x10000000
#define OPCODE_MASK		0x0F000000
#define MAX_ADR_MEM		0xFFFFFFFF
#define DST_ALU_MASK	0x00F80000
#define SRC_AR_MASK		0x0003E000
#define SRC_INT_MASK	0x00F00000
#define IMM_AR_MASK		0x0003FFFF
#define SRC_LOG_MASK	0x0007C000
#define ADR_LDST_MASK	DST_ALU_MASK
#define REG_LDST_MASK	SRC_LOG_MASK	
#define F_LDST_MASK		0x00003800
#define IMM_LDST_MASK	0x000003FF
#define REG_IMM_AR_MASK 0x00040000
#define LD_ST_MASK		0x00000400
#define DST_CALL_MASK	DST_ALU_MASK
#define IMM_CALL_MASK	0x0007FFFF
#define DST_IO_MASK		SRC_INT_MASK
#define SRC_IO_MASK		0x000F0000
#define DST_SH_MASK		DST_ALU_MASK
#define SRC_SH_MASK		SRC_LOG_MASK
#define IO_MASK			0x00008000
#define IMM_SH_MASK		0x00003E00
#define SH_L_R_MASK		0x00000100
#define DST_LDC_MASK	SRC_INT_MASK
#define IMM_LDC_MASK	0x0000FFFF
#define LDC_H_L_MASK	0x00080000
#define IO_IN_ADDR		0X00001000
#define IO_OUT_ADDR		0x00002000


#define INTR	0
#define ADD 1
#define SUB	2
#define MUL	3
#define DIV 4
#define CMP 5
#define AND 6
#define OR	7
#define NOT 8
#define TEST 9
#define LDR_STR 10
#define CALL 12
#define IN_OUT 13
#define MOV_SH 14
#define LDC 15


#define GPR_MAX 15
#define PC 16
#define LR 17
#define SP 18
#define PSW	19

#define EQ	0
#define NE	1
#define GT	2
#define GE	3
#define	LT	4
#define	LE	5
#define AL	7


class Instruction
{
public:
	char _con; 
	char _opCode; 
	int _sign; 
	char _src;
	char _dst;
	char _a;
	char _r;
	char _f;
	int _imm;
	bool _arImm; // Samo za aritmeticke instrukcije
	bool _load; //Samo za load instrukciju
	bool _in; // samo za in/out
	bool _shleft; //samo za mov/sh
	bool _ldHigh; 

	int _op1, _op2, _opImm; 
	Instruction() {}
	Instruction(char con, char opCode, int sign, char src, char dst, char a, char r, char f,
		int imm, bool arImm, bool load, bool in, bool shleft, bool ldHigh) :
		_con(con), _opCode(opCode), _sign(sign), _src(src), _dst(dst), _a(a), _r(r),
		_f(f), _imm(imm), _arImm(arImm), _load(load), _in(in), _shleft(shleft), _ldHigh(ldHigh)
	{
		
	
	
	}
				

	~Instruction();
};

