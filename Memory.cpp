#include "Memory.h"
#include <iostream>
using namespace std; 


Memory::Memory()
{
}


Memory::~Memory()
{
}

void Memory::writeSection(unsigned int address, vector<char> content)
{

	for (int i = 0; i < content.size(); i++) {
		physicalMemory[address + i] = content[i]; 
	}
}

void Memory::addValue(unsigned int address, int value)
{
	// koristi se za relokacije - za .long direktivu
	char byte1 = physicalMemory[address]; //najnizi bajt
	char byte2 = physicalMemory[address + 1];
	char byte3 = physicalMemory[address + 2];
	char byte4 = physicalMemory[address + 3];
	int memValue = 0;
	memValue |= byte1;
	memValue |= ((int)byte2 << 8);
	memValue |= ((int)byte3 << 16); 
	memValue |= ((int)byte4 << 24); 
	memValue += value; 
	writeDoubleWord(address, memValue); 
}

void Memory::subValue(unsigned int address, int value)
{
	char byte1 = physicalMemory[address]; //najnizi bajt
	char byte2 = physicalMemory[address + 1];
	char byte3 = physicalMemory[address + 2];
	char byte4 = physicalMemory[address + 3];
	int memValue = 0;
	memValue |= byte1;
	memValue |= ((int)byte2 << 8);
	memValue |= ((int)byte3 << 16);
	memValue |= ((int)byte4 << 24);
	memValue -= value;
	writeDoubleWord(address, memValue);

}

void Memory::addValue16B(unsigned int address, short value)
{
	unsigned char byte1 = physicalMemory[address]; //visih 8 iz inst ldc
	unsigned char byte0 = physicalMemory[address+1]; //nizih 8 iz instr ldc
	int memValue = 0; 
	memValue |= (byte1 << 8); 
	memValue |= byte0;
	memValue += value; 
	byte1 = (memValue >> 8) & 0xFF; 
	physicalMemory[address] = byte1; 
	byte0 = memValue & 0xFF; 
	physicalMemory[address + 1] = byte0; 

}

void Memory::writeByte(unsigned int address, char byte)
{
	physicalMemory[address] = byte; 
}

void Memory::writeWord(unsigned int address, short word)
{
	char byte1 = word & 0xFF; 
	char byte2 = (word >> 8) & 0xFF; 
	writeByte(address, byte1);
	writeByte(address+1, byte2); 

}

void Memory::writeDoubleWord(unsigned int address, int doubleWord)
{
	char byte1 = doubleWord & 0xFF;
	char byte2 = (doubleWord >> 8) & 0xFF;
	char byte3 = (doubleWord >> 16) & 0xFF; 
	char byte4 = (doubleWord >> 24) & 0xFF;
	writeByte(address, byte1);
	writeByte(address + 1, byte2);
	writeByte(address + 2, byte3);
	writeByte(address + 3, byte4); 
}

 unsigned int Memory::readInstruction(unsigned int address) {

	unsigned char byte1 = physicalMemory[address]; //najnizi bajt
	unsigned char byte2 = physicalMemory[address + 1];
	unsigned char byte3 = physicalMemory[address + 2];
	unsigned char byte4 = physicalMemory[address + 3];
	unsigned int instruction  = 0;
	instruction |= ((int)byte1 << 24); 
	instruction |= ((int)byte2 << 16);
	instruction |= ((int)byte3 << 8);
	instruction |= byte4;
	
	
	;
	return instruction; 
}

 int Memory::readDoubleWord(unsigned int address)
 {
	unsigned char byte1 = physicalMemory[address]; //najnizi bajt
	unsigned char byte2 = physicalMemory[address + 1];
	unsigned char byte3 = physicalMemory[address + 2];
	unsigned char byte4 = physicalMemory[address + 3];
	unsigned int doubleWord = 0;
	 doubleWord |= ((int)byte4 << 24);
	 doubleWord |= ((int)byte3 << 16);
	 doubleWord |= ((int)byte2 << 8);
	 doubleWord |= byte1;
	 


	 return doubleWord;
 }

 void Memory::pushDoubleWord(int doubleWord, unsigned int address)
 {
	 char byte1 = doubleWord & 0xFF;
	 char byte2 = (doubleWord >> 8) & 0xFF;
	 char byte3 = (doubleWord >> 16) & 0xFF;
	 char byte4 = (doubleWord >> 24) & 0xFF;
	 writeByte(address - 4, byte1);
	 writeByte(address - 3, byte2);
	 writeByte(address - 2, byte3);
	 writeByte(address - 1, byte4);
 }

 int Memory::popDoubleWord(unsigned int address)
 {
	unsigned char byte1 = physicalMemory[address]; //najnizi bajt
	unsigned char byte2 = physicalMemory[address + 1];
	unsigned char byte3 = physicalMemory[address + 2];
	unsigned char byte4 = physicalMemory[address + 3];
	unsigned int doubleWord = 0;
	doubleWord |= ((int)byte4 << 24);
	doubleWord |= ((int)byte3 << 16);
	doubleWord |= ((int)byte2 << 8);
	doubleWord |= byte1;
	
	return doubleWord;
 }


 void Memory::writeInIO(unsigned int address, int doubleWord) {
	 char byte1 = doubleWord & 0xFF;
	 char byte2 = (doubleWord >> 8) & 0xFF;
	 char byte3 = (doubleWord >> 16) & 0xFF;
	 char byte4 = (doubleWord >> 24) & 0xFF;
	 IOMemory[address] = byte1;
	 IOMemory[address + 1] = byte2;
	 IOMemory[address + 2] = byte3;
	 IOMemory[address + 3] = byte4;
 }

 int Memory::readFromIO(unsigned int address)
 {
	 char byte1 = IOMemory[address];	//najnizi bajt
	 char byte2 = IOMemory[address + 1];
	 char byte3 = IOMemory[address + 2];
	 char byte4 = IOMemory[address + 3];
	 int doubleWord = 0;
	 doubleWord |= byte1;
	 doubleWord |= ((int)byte2 << 8);
	 doubleWord |= ((int)byte3 << 16);
	 doubleWord |= ((int)byte4 << 24);
	 return doubleWord;
 }

 bool Memory::getKBStatus()
 {
	 char byte3 = IOMemory[KB_STATUS_ADDR + 1];
	 if (byte3 & 0x4)
		 return true;
	 else
		 return false; 
 }

