#pragma once
#include "Memory.h"
class KeyboardListener
{
public:
	volatile bool keyboardInt; 
	Memory* memory; 
	KeyboardListener(Memory* memory);
	~KeyboardListener();
};
void* keyboardListenerFunction(void* keyboardListenerV); 
