#include "StringParser.h"
#include "OutputElementReader.h"
#include "Errors.h"
#include <vector>
#include <string>
StringParser::StringParser()
{
}

StringParser::~StringParser()
{
}

unsigned int StringParser::from8(string num)
{
	unsigned int number = 0;
	int fact = 1;
	for (int i = num.length() - 1, fact = 1; i >= 0; --i, fact *= 8)
		if (num[i] >= '0' || num[i] <= '7')
			number += (num[i] - '0')*fact;
		else
			throw new Error("Pogresan format broja");
	return number;
}

unsigned int StringParser::from16(string num)
{
	unsigned int number = 0;
	int fact = 1;
	for (int i = num.length() - 1; i >= 0; --i, fact *= 16)
		if (num[i] >= '0' && num[i] <= '9')
			number += (num[i] - '0')*fact;
		else
			if (num[i] >= 'A' && num[i] <= 'F')
				number += (num[i] - 'A' + 10)*fact;
			else
				if (num[i] >= 'a' && num[i] <= 'f')
					number += (num[i] - 'a' + 10)*fact;
				else
					throw new Error("Pogresan format broja");
	return number;
}

unsigned int StringParser::from2(string num)
{
	unsigned int number = 0;
	int fact = 1;
	for (int i = num.length() - 1; i >= 0; --i, fact *= 2)
		if (num[i] >= '0' || num[i] <= '1')
			number += (num[i] - '0')*fact;
		else
			throw new Error("Pogresan format broja");
	return number;
}

unsigned int StringParser::from10(string num)
{
	unsigned int number = 0;
	int fact = 1;
	for (int i = num.length() - 1; i >= 0; --i, fact *= 10)
		number += (num[i] - '0')*fact;
	return number;
}
bool StringParser::isSeparator(char c) {
	if (c == ' ' || c == '\t' || c == ',' || c=='\n' || c=='\r' || c=='\r\n' || c=='\r')
		return true;
	else
		return false;
}

string StringParser::getWord(string& line) {
	string word;
	do {
		int start, end;
		for (start = 0; start < line.size() && isSeparator(line[start]); ++start);
		for (end = start; end < line.size() && !isSeparator(line[end]); ++end);
		word = line.substr(start, end - start);
		line = line.substr(end);
	} while (word.size() == 0 && line.size()>0);
	return word;

}

vector<string> StringParser::parseLine(string line) {

	vector<string> result;
	string word;
	while (line.size() > 0) {
		word = getWord(line);
		if (word != "")
			result.push_back(word);
	}
	return result;
}

Code StringParser::decode(string word)
{
	if (word[0] == 'T') return Code::SymbolTableId; 
	if (word[0] == '#' && word[1] == 'r') return Code::RelocationTableId; 
	if (word[0] == '#') return Code::SectionId; 
	return Code::WrongId; 
}

SymbolTableElement StringParser::createSymbolTableElement(vector<string> params)
{
	int num = stoi(params[0]);
	string name = params[1];
	int section = stoi(params[2]); 
	bool local = true;
	if (params[3] == "false")
		local = false;
	int value = stoi(params[4]); 
	int size = 0; 
	if (params.size() == 6)
		size = stoi(params[5]); 
	return SymbolTableElement(num, name, section, local, value, size);
}

RelocationTableElement StringParser::createRelocationTableElement(vector<string> params)
{
	int offset = stoi(params[0]); 
	string type = params[1]; 
	int reference = stoi(params[2]); 
	return RelocationTableElement(offset, type, reference);
}

string StringParser::getSectionNameFromSec(vector<string> params)
{
	string word = params.front(); //#.sectionName

	return word.substr(1); 
}

CodeScript StringParser::decodeScript(string word)
{
	if (word == ".") return LocationCounter; 
	if (word[0] == '.' && word.size() > 1) return SectionOrder; 
	return GlobalSymbol; 
}

unsigned int StringParser::parseToInt(string num)
{ 
	if (allNum(num)) {
		// u pitanju je ili decimalni ili oktalni broj
		if ((num[0] == '0') && num.size() > 1)
			return from8(num);
		else
			return from10(num);
	}
	else 
		if (num[0] == '0' && (num[1] == 'x' || num[1] == 'X'))
			return from16(num.substr(2));
	else 
		if (num[0] == '0' && (num[1] == 'b' || num[1] == 'B'))
			return from2(num.substr(2));
	throw new Error("Invalid number format"); 

}
bool StringParser::allNum(string num) {
	for (int i = 0; i < num.length(); ++i)
		if (!(num[i] >= '0' && num[i] <= '9'))
			return false;
	return true;
}

bool StringParser::isAlignOp(vector<string> params)
{
	/// . = ALIGN (param1, param2)
	/// global = ALIGN (param1, param2)
	unsigned int b = 5; 
	if (params.size() != b) return false;
	string param1 = params[2]; 
	string param2 = params[3]; 
	string param3 = params[4]; 

	if ((param1 == "ALIGN") && (param2[0] == '(')  && (param3[param3.size() - 1] == ')'))
		return true; 
	return false;  

}

string StringParser::getSectionNameFromRel(vector<string> params)
{
	string word = params.front(); //#rel.sectionName; 
	return word.substr(4); 
}
unsigned int StringParser::getSectionSize(vector<string> params){

	unsigned size = stoi(params[5]); 
	return size; 
}