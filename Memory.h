#pragma once
#include <vector>
#include <map>
using namespace std; 

#define KB_OUTPUT_ADDR 0x00002000
#define KB_INPUT_ADDR  0x00001000
#define KB_STATUS_ADDR 0x00001010
#define KB_CHAR_READY  0x00000400
class Memory
{
	const unsigned int MAX_ADDRESS = UINT32_MAX; 
public:

	Memory();
	~Memory();

	//IVT tabela se mapira na pocetak memory
	map<unsigned int, unsigned char> physicalMemory; 
	map<unsigned int, unsigned char> IOMemory; 




	void writeSection(unsigned int address, vector<char> content); 
	void addValue(unsigned int address, int value); 
	void subValue(unsigned int address, int value); 
	void addValue16B(unsigned int address, short value); 
	void writeByte(unsigned int address, char byte); 
	void writeWord(unsigned int address, short word); 
	void writeDoubleWord(unsigned int address, int doubleWord); 
	unsigned int readInstruction(unsigned int address); 
	int readDoubleWord(unsigned int address); 
	void pushDoubleWord(int doubleWord, unsigned int address); 
	int popDoubleWord(unsigned int address); 
	void writeInIO(unsigned int address, int doubleWord);
	int readFromIO(unsigned int address);
	bool getKBStatus(); 
};

