#include "VNContext.h"
#include <iostream> 
using namespace std; 




bool VNContext::Z()
{
	if (zero == nullptr) return false; 
	
	return (zero->result == 0); 
}

bool VNContext::N()
{
	if (negativ == nullptr)
		return false; 
	return (negativ->result < 0); 
}

bool VNContext::O()
{
	if (overflow == nullptr)
		return false; 
	switch (overflow->opCode) {
	case ADD:
		if (overflow->op1 > 0 && overflow->op2 > 0 && overflow->result <= 0)
			return true; 
		if (overflow->op1 < 0 && overflow->op2 < 0 && overflow->result >=0)
			return true; 
		return false; 
	case SUB: case CMP: 
		if (overflow->op1 > 0 && overflow->op2 < 0 && overflow->result <= 0)
			return true; 
		if (overflow->op1 < 0 && overflow->op2>0 && overflow->result >= 0)
			return true; 
		return false; 
	}
}

bool VNContext::C()
{
	
	if (carry == nullptr) return false; 
	unsigned int op1; 
	unsigned int op2; 
	switch (carry->opCode) {
	case MOV_SH :
		if (carry->op1 == 0 || carry->op2 == 0) return false; //Nije ni izvrseno nikakvo siftovanje
		if (carry->left) {
			int mask = (1 << (32 - carry->op2));
			if (carry->op1 & mask)
				return true;
			else
				return false;
		}
		else {
			int mask = 1 << (carry->op2 - 1);
			if (carry->op1 & mask)
				return true;
			else
				return false;
		} 

	case ADD:
		op1 = carry->op1; 
		op2 = carry->op2;
		if (op1 + op2 < op1 || op1 + op2 < op2)
			return true;
		else
			return false; 

	case SUB: 
		op1 = carry->op1; 
		 op2 = carry->op2; 
		if (op1 < op2)
			return true;
		else
			return false; 

	}

}

void VNContext::flags(char opCode, int op1, int op2, int result, bool left)
{
	// ako smo dosli da postavimo flagove, Z i N uvek postavljamo
	if (zero == nullptr)
		zero = new LazyFlag();
	zero->opCode = opCode;
	zero->op1 = op1;
	zero->op2 = op2;
	zero->result = result;

	if (negativ == nullptr)
		negativ = new LazyFlag();
	negativ->opCode = opCode;
	negativ->result = result;
	negativ->op1 = op1;
	negativ->op2 = op2;
	switch (opCode) {

	case ADD: case SUB: case CMP: 

		if (overflow == nullptr)
			overflow = new LazyFlag(); 
		overflow->opCode = opCode;
		overflow->result = result;
		overflow->op1 = op1;
		overflow->op2 = op2;
		break; 

	case MOV_SH:

		if (carry == nullptr)
			carry = new LazyFlag(); 
		carry->opCode = opCode;
		carry->result = result;
		carry->op1 = op1;
		carry->op2 = op2;
		carry->left = left; 
		break; 
	}
}

void VNContext::setPSW()
{
	if (O())
		reg[PSW] |= O_FLAG;
	if (Z())
		reg[PSW] |= Z_FLAG;
	if (O())
		reg[PSW] |= O_FLAG; 
	if (C())
		reg[PSW] |= C_FLAG; 
}

bool VNContext::isEnabledTimer() {
	return reg[PSW] & TIMER_FLAG;
}



bool VNContext::isEnabledInt()
{
	return reg[PSW] & INT_FLAG; 
}

void VNContext::disableInterrupts()
{
	reg[PSW] &= CLEAR_FLAGS; 
}

void VNContext::resetFlags()
{
	reg[PSW] &= RESET_FLAGS; 
	reg[PSW] |= TIMER_FLAG; 
	reg[PSW] |= INT_FLAG; 

}



VNContext::VNContext()
{
	carry = nullptr; 
	zero = nullptr; 
	negativ =nullptr; 
	 overflow = nullptr; 
}


VNContext::~VNContext()
{
	if (carry != nullptr) {
		delete carry;
		carry = nullptr; 
	}
	if (zero != nullptr) {
		delete zero; 
		zero = nullptr; 
	}
	if (negativ != nullptr) {
		delete negativ; 
		negativ = nullptr; 
	}
	if (overflow != nullptr) {
		delete overflow; 
		overflow = nullptr; 
	}

}
