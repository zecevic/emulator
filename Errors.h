#pragma once
#include <string>
#include <iostream>
using namespace std; 
class Error {
public: 
	Error(string msg); 
	string errorMsg; 

	friend ostream & operator<<(ostream &os, const Error& a) {
		return os << a.errorMsg <<endl;
	}

	
};