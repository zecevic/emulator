#include "ReaderObj.h"
#include <string>
#include <list>
#include <fstream>
#include "StringParser.h"
#include "Errors.h"
#include "OutputElementReader.h"
ReaderObj::ReaderObj()
{
}


ReaderObj::~ReaderObj()
{
}

OutputElementReader ReaderObj::readFile(char* fileName)
{
	StringParser parser; 
	if (fileName == nullptr) throw new Error("Nevalidno ime file-a");
	ifstream inputFile; 
	inputFile.open(fileName, ios::in);
	if (!inputFile.is_open()) throw new Error("Ne postoji uneti file");

	
	string file(fileName); 
	OutputElementReader outputObject(file); 

	string line; 
	while (!inputFile.eof()) {
		getline(inputFile, line);
		vector<string> result = parser.parseLine(line);
		if (result.empty()) continue; 
		Code code = parser.decode(result.front()); 

		if (code == Code::SymbolTableId) {
			getline(inputFile, line); 
			while (true && !inputFile.eof()) {

				getline(inputFile, line);
				result = parser.parseLine(line);
				if (result.empty()) break; //Prazna linije je razdvajanje delova izlaznog fajla
				if (result.size() != 6 && result.size()!=5)
					throw new Error("Pogrsan format ulaza u tabeli simbola"); //Ovde treba neki error

				SymbolTableElement ste = parser.createSymbolTableElement(result);
				outputObject.addSymbolTableElement(ste);
			}
		}
		else if (code == Code::RelocationTableId) {
			string section = parser.getSectionNameFromRel(result); // iz tekuce linije izvucemo za koju sekciju je relokacija
			RelocationTable* rel = outputObject.createNewRelocationTable(section); 
			getline(inputFile, line);

			while (true && !inputFile.eof()) {
				
				getline(inputFile, line); 
				result = parser.parseLine(line); 
				if (result.empty()) break; 
				if (result.size() != 3)
					throw new Error("Pogresan format ulaza u tabeli relokacija"); 
				RelocationTableElement rte =  parser.createRelocationTableElement(result); 
				rel->addRelocation(rte); 
			}
		}
		else if (code == Code::SectionId) {
			string sectionName = parser.getSectionNameFromSec(result); 
			Section* section = outputObject.createNewSection(sectionName); 
			section->fileName = fileName; 
			section->size = outputObject.getSectionSize(sectionName); 
			int num = outputObject.getSectionNum(sectionName); 
			section->num = num; 
			unsigned int size = 0;
			while (true && !inputFile.eof()) {
				getline(inputFile, line); 
				result = parser.parseLine(line); 
				if (result.empty()) break; 
				char value1 = 0; 
				char value2 = 0; 
				int k = 0; 
				
				for (int i = 0; i < line.size(); i++)
					if (line[i] == ' ' || line[i] == '\0' || line[i] == '\t' || line[i]=='\n') continue;
					else {
						string str = line.substr(i, 1); 
						int v = stoi(str, nullptr, 16); //Procitam char, i prebacim ga u int jer su to vrednosti od 0-9 i od A-F
						k++; 
						if (k == 1)
							 value1 = v & 0xF;
						else {
							value2 = v & 0xF; 
							unsigned char value = (value1 << 4) | value2; 
							section->addContent(value); //ovo je jedan byte
							k = 0;
							size++; 
						}
					}
			}
			
		}

	}
	return outputObject; 
}
