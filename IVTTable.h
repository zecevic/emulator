#pragma once
#include "Memory.h"
#define ILEGAL_INSTRACTION_INT 2
#define TIMER_INT 1
#define RESET_INT 0
#define KB_INT 3
#define ENTRY_SIZE 4
class IVTTable
{
	Memory* _memory; 
public:
	IVTTable(Memory* memory);
	~IVTTable();
	unsigned int getRoutine(unsigned int entry); 
};

