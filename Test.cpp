#include "OutputElementReader.h"
#include "ReaderObj.h"
#include "StringParser.h"
#include "ReaderScript.h"
#include "Memory.h"
#include <iostream>
#include "Emulator.h"
#include "Errors.h"
#include <vector>
#include <stdlib.h>
using namespace std; 

int main(int argc, char* argv[]) {

	// Mora da se unese bar jedan file i jedan skript

	if (argc<3) {
		cout << "Niste uneli dovoljan broj argumenata" << endl;
		return -1;
	}

	try{
		ReaderObj reader;
		Memory* m = new Memory();
		vector<OutputElementReader> obj;
		for (int i = 1; i < argc - 1; i++) {
			OutputElementReader o = reader.readFile(argv[i]);
			obj.push_back(o); 
		}
		char* scriptName = argv[argc-1];

		ReaderScript r(m); 
		r.combineFiles(scriptName, obj); 
		VNContext* context = new VNContext(); 
		Emulator emulator(m, context, r.mainAddr);
		emulator.emulate(); 
		return 0; 

	emulator.emulate(); 
	} catch (Error* e){
		cout<<(*e); 
	}
	system("pause"); 
}