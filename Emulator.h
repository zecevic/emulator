#pragma once
#include "Instruction.h"
#include "Memory.h"
#include "VNContext.h"
#include "IVTTable.h"
#include "KeyboardListener.h"
#include <ctime>
#include <pthread.h>

using namespace std;

class Emulator
{
	Memory* _memory; 
	bool doEmulate; 
	bool timerInt; 
	bool consoleOutput; 
	bool ilegalInstruction; 
	bool initializing; 
	VNContext* _context; 
	IVTTable* ivt; 
	clock_t startTime, endTime; 
	double counter = 1;
	unsigned int _mainAddr; 
	int brojac; 
	pthread_t keyboardThread;
	volatile KeyboardListener* keyboardListener;	
		
	
	

public:


	Emulator(Memory* memory, VNContext* context, unsigned int mainAddr) {
		_memory = memory; 
		_context = context; 
		_mainAddr = mainAddr; 

		ivt = new IVTTable(memory); 
	
		keyboardListener= new KeyboardListener(memory); 
		pthread_create(&keyboardThread, NULL, &keyboardListenerFunction, (void*)keyboardListener); 
	}


	~Emulator() {}
	void emulate(); 
	void handleInterrupts(); 
	Instruction decodeInstruction(unsigned int opCode); 
	void executeInstruction(Instruction instruction); 
	void executeINT(Instruction ins); 
	void executeADD(Instruction ins); 
	void executeSUB(Instruction ins); 
	void executeMUL(Instruction ins); 
	void executeDIV(Instruction ins); 
	void executeCMP(Instruction ins); 
	void executeAND(Instruction ins);
	void executeOR(Instruction ins);
	void executeNOT(Instruction ins);
	void executeTEST(Instruction ins);
	void executeLDR_STR(Instruction ins); 
	void executeCALL(Instruction ins); 
	void executeIN_OUT(Instruction ins); 
	void executeMOV_SH(Instruction ins); 
	void executeLDC(Instruction ins); 
	int pop();
	void push(int word); 

	void restartCPU(); 
};

