#include "IVTTable.h"



IVTTable::IVTTable(Memory* memory)
{
	_memory = memory; 
}


IVTTable::~IVTTable()
{

}

unsigned int IVTTable::getRoutine(unsigned int entry)
{
	unsigned int routine = _memory->readDoubleWord(entry*ENTRY_SIZE); 
	return routine; 
}
