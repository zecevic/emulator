#include "ReaderScript.h"
#include "Errors.h"
#include "StringParser.h"
#include "OutputElementReader.h"
#include <fstream>
#include <string>
#include <iostream>
#include "Memory.h"
using namespace std; 
 
ReaderScript::ReaderScript(Memory* mem)
{
	this->mem = mem; 
}
ReaderScript::~ReaderScript()
{
}

void ReaderScript::combineFiles(char* scriptName, vector<OutputElementReader> objFiles)
{
	StringParser parser;
	//Bvector<ScriptGlobalSymbol> globalSymbols;
	ifstream scriptFile;
	scriptFile.open(scriptName, ios::in);
	int size = objFiles.size();
	string scriptLine;
	locationCounter = 0;
	linkedSections = vector<Section>(); //prazan vektor linkovanih sekcija
	linkedSymbolTable = vector<SymbolTableElement*>(); 
	int cnt = 0;
	while (!scriptFile.eof()) {

		getline(scriptFile, scriptLine);
		vector<string> result = parser.parseLine(scriptLine);
		if (result.empty()) continue; 
	
		CodeScript code = parser.decodeScript(result[0]);

		if (code == CodeScript::LocationCounter) {

			unsigned int newLocationCounter;
			newLocationCounter = executeLocationCnt(result);
			if (newLocationCounter < locationCounter)
				throw new Error("Nevalidna vrednost za locationCounter");
			locationCounter = newLocationCounter; 

		}
		else if (code == CodeScript::SectionOrder) {
			//naci sekciju, i postaviti njen sadrzaj u memoriju
			//uvecati locationCOunter
			//memory.put(locationCOunter, section); 
			//LocationCounter+=section.size(); 

			for (int i = 0; i < objFiles.size(); i++) {

				string sectionName = result[0];
				Section* section = objFiles[i].getSection(sectionName);
				if (section != nullptr) {


					

					vector<char> memContent; 
					if (section->type == ".bss") {
						memContent = vector<char>(section->size);
					}
					else
						memContent = section->_content;
					
					mem->writeSection(locationCounter, memContent);
					
					section->startAddr = locationCounter;
					section->inMemory = true;
					locationCounter += section->size;
					


					SymbolTableElement* ste = new SymbolTableElement();
					cnt++;
					ste->_section = section->num;
					ste->_num = cnt;
					ste->_local = false;
					ste->_name = sectionName + "." + section->fileName;
				//	ste->_name = sectionName; 
					ste->_size = section->size;
					ste->_value = section->startAddr;
					linkedSymbolTable.push_back(ste);


					vector<SymbolTableElement*> symTable = objFiles[i].getSymbolTable(); 

					
					for (int j = 0; j < objFiles[i]._symbolTable.size(); j++) 
						if (objFiles[i]._symbolTable[j]->_section == section->num) {
							// ako je simbol iz te sekcije, dodamo adresu
							objFiles[i]._symbolTable[j]->_value += section->startAddr;
							
							if (!objFiles[i]._symbolTable[j]->_local) {
								//ako je simbol globalan dodamo ga u globalnu-linkovanu tabelu simbola
								
								string symName = objFiles[i]._symbolTable[j]->_name; 
						//		if (symName == sectionName)
							//		symName = sectionName + "." + section->fileName; 
								if (globalDefined(symName))
									throw new Error("Redefinicija simbola!"); 
								cnt++;
								SymbolTableElement* ste = new SymbolTableElement();
								ste->_section = section->num;
								ste->_num = cnt; 
								ste->_local = false; 
								ste->_name = symName; 
								ste->_size = objFiles[i]._symbolTable[j]->_size;
								ste->_value = objFiles[i]._symbolTable[j]->_value; 
								linkedSymbolTable.push_back(ste);


							}
						}
					if (section->_relocationTable)
						for (int j = 0; j < section->_relocationTable->size; j++) {
							section->_relocationTable->_relocations[j]._offset += section->startAddr; 
					}
					
				}
			}

		}
		else if (code == CodeScript::GlobalSymbol) {
			//proveriti da vec negde ne postoji definisan ovaj simbol 
			//ako ne postoji dodati gao globalni simbol
			// new GlobalSymbol(name, size); 
			string name = result[0];
			for (int i = 0; i < objFiles.size(); i++) {
				SymbolTableElement* sym = objFiles[i].getSymbolTableElementByName(name);
				if (sym!=nullptr)
					if (sym->_section != 0) throw new Error("Vec postoji simbol! Redefinicija nije dozvoljena");
				
			}
			for (int i = 0; i < globalSymbols.size(); i++) {
				if (globalSymbols[i].name == name) throw new Error("Vec postoji simbol!Redefinicija nije dozvoljena");
			}
			//Ako je doslo dovde onda nije nijedna prethodna situacija
			int value = executeGlobalSym(result);
			ScriptGlobalSymbol gs;
			gs.name = name;
			gs.value = value;
			globalSymbols.push_back(gs);

		}
	}

	//kada je obradjen ceo script file, traba ubaciti i ostale sekcije u memoriju
	//resiti sve relokacije i nedefinisane simbole
	vector<Section*> sections;
	for (int i = 0; i < objFiles.size(); i++) {
		for (int j = 0; j < objFiles[i]._sections.size(); j++)
			if (!objFiles[i]._sections[j]->inMemory)
				sections.push_back(objFiles[i]._sections[j]);
	}

	sections = sortSectionsByName(sections);

	vector<char> memContent; 
	for (int i = 0; i < sections.size(); i++) {
		if (sections[i]->type == ".bss") {
			memContent = vector<char>(sections[i]->size); 
		} else 
			memContent = sections[i]->_content;
		
		//	put Section contnet in Memory;
		mem->writeSection(locationCounter, memContent);
	
		sections[i]->inMemory = true;
		sections[i]->startAddr = locationCounter;

		SymbolTableElement* ste = new SymbolTableElement();
		ste->_section = sections[i]->num;
		cnt++;
		ste->_num = cnt;
		ste->_local = false;
		ste->_name = sections[i]->name + "." + sections[i]->fileName;
//		ste->_name = sections[i]->name; 
		ste->_size = sections[i]->size;
		ste->_value = sections[i]->startAddr;
		linkedSymbolTable.push_back(ste);


		vector<SymbolTableElement*> symTable = objFiles[i].getSymbolTable();
		for (int k = 0; k < objFiles.size(); k++) {
			if (objFiles[k].fileName != sections[i]->fileName) continue; 
			for (int j = 0; j < objFiles[k]._symbolTable.size(); j++)
				if (objFiles[k]._symbolTable[j]->_section == sections[i]->num) {
					// ako je simbol iz te sekcije, dodamo adresu
					objFiles[k]._symbolTable[j]->_value += sections[i]->startAddr;
					if (!objFiles[k]._symbolTable[j]->_local) {
						//ako je simbol globalan dodamo ga u globalnu-linkovanu tabelu simbola
						//Ako je doslo dovde onda nije nijedna prethodna situacija
						string symName = objFiles[k]._symbolTable[j]->_name;
						if (globalDefined(symName))
							throw new Error("Redefinicija simbola!");
						cnt++;
						SymbolTableElement* ste = new SymbolTableElement();
						ste->_section = sections[i]->num;
						ste->_num = cnt;
						ste->_local = false;
						ste->_name = symName;
						ste->_size = objFiles[k]._symbolTable[j]->_size;
						ste->_value = objFiles[k]._symbolTable[j]->_value;
						linkedSymbolTable.push_back(ste);


					}
				}
		}
		if (sections[i]->_relocationTable!=NULL)
			for (int j = 0; j < sections[i]->_relocationTable->size; j++) {
				sections[i]->_relocationTable->_relocations[j]._offset += sections[i]->startAddr;
			}
		locationCounter += sections[i]->size;
	}

	// sada proveriti da li su svi simboli definisani
	for (int i = 0; i < objFiles.size(); i++) {
		for (int j = 0; j < objFiles[i]._symbolTable.size(); j++) {
			if (objFiles[i]._symbolTable[j]->_section == 0 && objFiles[i]._symbolTable[j]->_name!="UND")		
				if (!globalDefined(objFiles[i]._symbolTable[j]->_name))
					throw new Error("Postoje nedefinisani simboli, nemoguce je povezati fajlove"); 
		}
	}

	//sada srediti relokacije
	for (int i = 0; i< objFiles.size(); i++)
		for (int j = 0; j<objFiles[i]._sections.size(); j++)
			if (objFiles[i]._sections[j]->_relocationTable!=NULL)
			for (int k = 0; k < objFiles[i]._sections[j]->_relocationTable->size; k++) {
				unsigned int offset = objFiles[i]._sections[j]->_relocationTable->_relocations[k]._offset;
				int reference = objFiles[i]._sections[j]->_relocationTable->_relocations[k]._reference; 
				SymbolTableElement* symbol = objFiles[i].getSymbolTableElementByNum(reference); 
				string name = symbol->_name; 
				string type = objFiles[i]._sections[j]->_relocationTable->_relocations[k]._type;  //"R_386_16L", "R_386_16H"
				if (symbol->_num == symbol->_section)
					name = name + "." + objFiles[i]._sections[j]->fileName; 
				unsigned int value = getValueGlbSymbol(name); 
				if (type == "R_386_32_PLUS")
					mem->addValue(offset, value);
				else
					if (type == "R_386_32_MINUS") 
						mem->subValue(offset, value); 
					else if (type == "R_386_16H") {
						short highV = (value >> 16) & 0xFFFF; 
						mem->addValue16B(offset, highV); 
					}
					else if (type == "R_386_16L") {
						short lowV = value & 0xFFFF; 
						mem->addValue16B(offset, lowV); 
					}
			}


	///pronaci vrednost main -a
	bool found = false; 
	for (int i = 0; i < linkedSymbolTable.size(); i++) {
		if (linkedSymbolTable[i]->_name == "main") {
			found = true; 
			mainAddr = linkedSymbolTable[i]->_value; 
		}
		
	}
	for (int i = 0; i < globalSymbols.size(); i++) {
		if (globalSymbols[i].name == "main") {
			found = true;
			mainAddr = globalSymbols[i].value;
		}

	}



}

unsigned int ReaderScript::executeLocationCnt(vector<string> result)
{
	// . = global
	// . = global + 2
	// . = global1 + global2
	// . = 12345

	StringParser parser; 
	string word = result[0]; 
	unsigned int value1 = 0, value2 = 0;
	unsigned int cons; 

	if (parser.isAlignOp(result)) {
		string op1, op2;
		// . = ALIGN (param1, param2)
		op1 = result[3].substr(1, result[3].size() - 1); 
		op2 = result[4].substr(0, result[4].size() - 1); 

		value1 =  parser.parseToInt(op1); 
		value2 = parser.parseToInt(op2); 
		if (value1%value2) {
			unsigned int mod = value1%value2; 
			value1 += (value2 - mod); 
		}
		return value1; 
	}
	else {

		if (isGlobalSymbol(result[2]))
			value1 = findValueGlbSymbyName(result[2]);
		else
			value1 =  parser.parseToInt(result[2]); //Posto je broj u pitanju, stavljamo u unsigned 

		if (result.size() == 3) return value1;

		if (result.size() != 5) throw new Error("Nevalidna dodela location counteru"); 

		//result[4] drugi operand
		if (isGlobalSymbol(result[4]))
			value2 = findValueGlbSymbyName(result[4]);
		else
			value2 = parser.parseToInt(result[4]); //Posto je broj u pitanju, stavljamo u unsigned 

		if (result[3] == "-")
			return value1 - value2;
		else if (result[3] == "+")
			return value1 + value2; 

		//ako nije nista od ovoga, onda je greska 
		throw new Error("Nevalidna dodela Location counteru"); 

	}
}

unsigned int ReaderScript::executeGlobalSym(vector<string> result)
{
	// global1 = global
	// global1 = global + 2
	// global3 = global1 - global2
	// global = 12345


	StringParser parser;
	string word = result[0];
	unsigned int value1 = 0, value2 = 0;
	unsigned int cons;

	if (parser.isAlignOp(result)) {
		string op1, op2;
		// . = ALIGN (param1, param2)
		op1 = result[3].substr(1, result[3].size() - 2);
		op2 = result[3].substr(0, result[4].size() - 1);
		value1 = (unsigned) parser.parseToInt(op1);
		value2 = (unsigned) parser.parseToInt(op2);
		if (value1%value2) {
			int mod = value1%value2;
			value1 += (value2 - mod);
		}
		return value1;
	}
	else {

		try {

			if (isGlobalSymbol(result[2]))
				value1 = findValueGlbSymbyName(result[2]);
			else
				value1 = parser.parseToInt(result[2]);
		}
		catch (Error* e) {
			throw new Error("Nevalidna definicija globalnog simbola");
		}

		if	(result.size() == 3) return value1;

		if (result.size() != 5) throw new Error("Nevalidna definicija globalnog simbola");


		try{ 
		//result[4] drugi operand
			if (isGlobalSymbol(result[4]))
				value2 = findValueGlbSymbyName(result[4]);
			else
				value2 = parser.parseToInt(result[4]);
		}
		catch (Error *e) {
			throw new Error("Nevalidna definicija globalnog simbola");
		}

		if	(result[3] == "-")
			return value1 - value2;
		else if (result[3] == "+")
			return value1 + value2;

		//ako nije nista od ovoga, onda je greska 
		throw new Error("Nevalidna definicija globalnog simbola");

	}

}

bool ReaderScript::isGlobalSymbol(string symbol)
{
	for (int i = 0; i < globalSymbols.size(); i++)
		if (globalSymbols[i].name == symbol) return true; 
	//
	for (int i = 0; i < linkedSymbolTable.size(); i++)
		if (linkedSymbolTable[i]->_name == symbol) return true; 
	return false;
}

unsigned int ReaderScript::findValueGlbSymbyName(string name)
{
	for (int i = 0; i < globalSymbols.size(); i++)
		if (globalSymbols[i].name == name) return globalSymbols[i].value; 
	for (int i = 0; i < linkedSymbolTable.size(); i++)
		if (linkedSymbolTable[i]->_name == name) return linkedSymbolTable[i]->_value;

	throw new Error("Pogresna dodela vrednosti. Nerazreseni simboli."); 
}

vector<Section*> ReaderScript::sortSectionsByName(vector<Section*> sections) {

	if (sections.size() == 0 || sections.size() == 1) return sections; 
	for (int i = 0; i<sections.size()-1; i++)
		for (int j = i + 1; j < sections.size(); j++) {
			int k = sections[i]->name.compare(sections[j]->name); 
			if (k > 0) {
				Section* temp = sections[i]; 
				sections[i] = sections[j]; 
				sections[j] = temp; 
			}
		}
	return sections; 
}

bool ReaderScript::globalDefined(string symbol)
{
	for (int i = 0; i < globalSymbols.size(); i++)
		if (globalSymbols[i].name == symbol) return true; 
	for (int i = 0; i < linkedSymbolTable.size(); i++)
		if (linkedSymbolTable[i]->_name == symbol) return true; 
	return false; 
}
unsigned int ReaderScript::getValueGlbSymbol(string name) {

	for (int i = 0; i < globalSymbols.size(); i++)
		if (globalSymbols[i].name == name)
			return globalSymbols[i].value; 
	for (int i = 0; i < linkedSymbolTable.size(); i++)
		if (linkedSymbolTable[i]->_name == name)
			return linkedSymbolTable[i]->_value;	
	throw new Error("Nedefinisan simbol"); 
}